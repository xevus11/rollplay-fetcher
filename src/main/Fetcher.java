package main;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
public class Fetcher {
	/**
	 * Null constructor.
	 */
	private Fetcher() {
	}
	/**
	 * Main method.
	 *
	 * @param args
	 *            Startup arguments (unused)
	 */
	public static void main(String[] args) {
		
		String values = questionPane();
		
		String link = values;
		reader(link);
	}
	
	/**
	 * creates an entry panel for url and token
	 * @return
	 * 		a string array with the url in the first position and the token in the second
	 */
	public static String questionPane() {
		JTextField url = new JTextField();
		Object[] message = { "url:", url};
		String values = "";
		int option = JOptionPane.showConfirmDialog(null, message, "Fetcher",
				JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
			values = url.getText();
		}else {
			System.exit(0);
			//probably bad code, but ech
		}
		return values;
	}
	
	/**
	 * Create an error pane with the given error.
	 *
	 * @param error
	 *            the error message to be shown.
	 */
	public static void errorPane(String error) {

		JOptionPane errorPane = new JOptionPane();

		JOptionPane.showMessageDialog(errorPane, error, "Error",
				JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * reads urls from the rss feed at the provided address, and automatically downloads files not present in its folder.
	 * @param url
	 * 			rss feed URL
	 */
	private static void reader(String url) {
		assert url.equals(null) : "error: null url";
		try {
			URL rssURL = new URL(url);
			BufferedReader in = new BufferedReader(new InputStreamReader(rssURL.openStream()));
			String source = "";
			Map<String, String> links = new LinkedHashMap<String, String>();
			Set<String> episodes = new HashSet<String>();
			source = in.readLine();
			while(source != null) {
				if(source.contains("<item>")) {
					source = in.readLine();
					String episodeName = "";
					String link = "";
					while(source != null && episodeName.equals("") && link.equals("")) {
						source = in.readLine();
						if(source != null && source.contains("title")) {
							episodeName = (source.substring((source.indexOf('>')+1), source.indexOf("</title>")));
							while(source != null && link.equals("")) {
								source = in.readLine();
								if(source != null && source.contains("enclosure url")) {
									link = (source.substring((source.indexOf('\"')+1), source.indexOf("\" type")));
								}
							}
						}
					}
					links.put(episodeName, link);
					episodes.add(episodeName);
				}
				source = in.readLine();
			}
			in.close();
			final Standby message = new Standby();
			message.start();
			for(String s : episodes) {
				File current = new File(s);
				if(!current.isFile() && !current.canRead()) {
					System.out.println(decode(links.get(s)));
					URL website = new URL(decode(links.get(s)));
					HttpsURLConnection connect = (HttpsURLConnection) website.openConnection();
					connect.setInstanceFollowRedirects(true);
					connect.connect();
					ReadableByteChannel rbc = Channels.newChannel(connect.getInputStream());
					FileOutputStream fos = new FileOutputStream(s);
					fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
					fos.close();
				}
			}
		}catch(MalformedURLException e) {
			errorPane(e.getMessage());
			e.printStackTrace();
		}
		catch(Exception e) {
			errorPane(e.getMessage());
			e.printStackTrace();
		}
		System.exit(0);
	}
	
	/**
	 * decodes a url so that it may be used.
	 * @param value
	 * 		string to be decoded
	 * @return
	 * 		a string safe to use as a url
	 * @throws UnsupportedEncodingException
	 */
	private static String decode(String value) throws UnsupportedEncodingException {
	    //return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
	    return value.replace("&amp;", "&");
	    //Not sure the above will work with everything, but it seems to work for all current links.
	}
}
