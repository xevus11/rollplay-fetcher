package main;
import javax.swing.JOptionPane;

public class Standby extends Thread{
	Standby(){
		
	}
	
	/**
	 * creates a warning dialogue that will close the program when closed.
	 */
	@Override
	public void run() {
		JOptionPane.showMessageDialog(null, "Please stand by while your files are downloaded. This message will close on completion. To cancel the operation, press \"OK.\"", "Fetcher", JOptionPane.WARNING_MESSAGE);
		System.exit(0);
		//only reaches exit if ok is pressed
	}
}
